@extends('layouts.dashboard')

@section('dashboard')
<div class="panel panel-default">

  <div class="panel-heading">
    {{ trans('dashboard.Comments') }}
  </div>

  <table class="table">
    <thead>
      <tr>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ID<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'id', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'id', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Author') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'author', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'author', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Comment\'s Topic') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'topic', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'topic', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          {{ trans('dashboard.Comment') }}
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Created At') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'created_at', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'comments', 'orderby' => 'created_at', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          {{ trans('dashboard.Operations') }}
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach($comments as $comment)
        <tr>
          <td>
            {{ $comment->id }}
          </td>
          <td>
            {{ $comment->author }}
          </td>
          <td>
            {{ $comment->topic }}
          </td>
          <td>
            {{ $comment->comment }}
          </td>
          <td>
            {{ $comment->created_at }}
          </td>
          <td>
            <a href="{{ route('admin.delete', ['table' => 'comments', 'where' => 'comment', 'what' => $comment->comment]) }}" title="{{ trans('dashboard.Delete Comment') }}" class="glyphicon glyphicon-remove font" aria-hidden="true"></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
