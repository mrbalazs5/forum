@extends('layouts.dashboard')

@section('dashboard')
<div class="panel panel-default">

  <div class="panel-heading">
    {{ trans('dashboard.Users') }}
  </div>

  <table class="table">
    <thead>
      <tr>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ID<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'id', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'id', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Username') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'username', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'username', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.First Name') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'fname', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'fname', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Last Name') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'lname', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'lname', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">E-Mail<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'email', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'email', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Permission') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'permission', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'permission', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          <span class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Topics') }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'topics', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                <li><a href="{{ route('admin.order', ['table' => 'users', 'orderby' => 'topics', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
              </ul>
          </span>
        </th>
        <th>
          {{ trans('dashboard.Operations') }}
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>
            {{ $user->id }}
          </td>
          <td>
            {{ $user->username }}
          </td>
          <td>
            {{ $user->fname }}
          </td>
          <td>
            {{ $user->lname }}
          </td>
          <td>
            {{ $user->email }}
          </td>
          <td>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $user->permission }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li>{{ trans('dashboard.Change Permission') }}</li>
                  <li role="separator" class="divider"></li>
                    <li><a href="{{ route('admin.change', ['table' => 'users', 'where' => 'username', 'is' => $user->username, 'what' => 'permission', 'to' => 'admin']) }}">Admin</a></li>
                    <li><a href="{{ route('admin.change', ['table' => 'users', 'where' => 'username', 'is' => $user->username, 'what' => 'permission', 'to' => 'user']) }}">User</a></li>
                </ul>
            </span>
          </td>
          <td>
            {{ $user->topics }}
          </td>
          <td>
            <a href="{{ route('admin.delete', ['table' => 'users', 'where' => 'username', 'what' => $user->username]) }}" title="{{ trans('dashboard.Delete User') }}" class="glyphicon glyphicon-remove font" aria-hidden="true"></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
