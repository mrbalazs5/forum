@extends('layouts.dashboard')

@section('dashboard')
  <h1>{{ trans('dashboard.Welcome to the Dashboard!') }}</h1>
  <h2>{{ trans('dashboard.Some statistics') }}</h2>
  <h3>{{ trans('dashboard.Number of users') }}: {{ DB::table('users')->count() }}</h3>
  <h3>{{ trans('dashboard.Number of topics') }}: {{ DB::table('topics')->count() }}</h3>
  <h3>{{ trans('dashboard.Number of comments') }}: {{ DB::table('comments')->count() }}</h3>
@endsection
