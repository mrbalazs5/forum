@extends('layouts.dashboard')

@section('dashboard')
<div class="panel panel-default">

  <div class="panel-heading">
    {{ trans('dashboard.Topics') }}
  </div>

  <table class="table">
    <thead>
        <tr>
          <th>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ID<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'id', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'id', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
                </ul>
            </span>
          </th>
          <th>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Topic\'s name') }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'name', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'name', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
                </ul>
            </span>
          </th>
          <th>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Topic\'s Author') }}r<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'author', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'author', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
                </ul>
            </span>
          </th>
          <th>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Topic\'s Category') }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'category', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'category', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
                </ul>
            </span>
          </th>
          <th>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('dashboard.Created At') }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'created_at', 'order' => 'desc']) }}">{{ trans('dashboard.Descending Order') }}</a></li>
                  <li><a href="{{ route('admin.order', ['table' => 'topics', 'orderby' => 'created_at', 'order' => 'asc']) }}">{{ trans('dashboard.Ascending Order') }}</a></li>
                </ul>
            </span>
          </th>
          <th>
            {{ trans('dashboard.Operations') }}
          </th>
        </tr>
    </thead>
    <tbody>
      @foreach($topics as $topic)
        <tr>
          <td>
            {{ $topic->id }}
          </td>
          <td>
            {{ $topic->name }}
          </td>
          <td>
            {{ $topic->author }}
          </td>
          <td>
            <span class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans("home.$topic->category") }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li>{{ trans('dashboard.Change Category') }}</li>
                  <li role="separator" class="divider"></li>
                  @foreach(DB::table('subcategories')->get() as $category)
                    <li><a href="{{ route('admin.change', ['table' => 'topics', 'where' => 'name', 'is' => $topic->name, 'what' => 'category', 'to' => $category->name]) }}">{{ trans("home.$category->name") }}</a></li>
                  @endforeach
                </ul>
            </span>
          </td>
          <td>
            {{ $topic->created_at }}
          </td>
          <td>
            <a href="{{ route('admin.delete', ['table' => 'topics', 'where' => 'name', 'what' => $topic->name]) }}" title="{{ trans('dashboard.Delete Topic') }}" class="glyphicon glyphicon-remove font" aria-hidden="true"></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

</div>
@endsection
