@extends('layouts.master')

@section('title')
  {{ trans('home.Home') }}
@endsection

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
@endsection

@section('content')
  <div class="row">
    <div id="categories" class="col-xs-10 categories scroll" style="padding-right: 20px;">
      <h1 class="text-center">{{ trans('home.welcome') }}</h1>
      <hr>
      @if(Session::has('deleted'))
        <div class="alert alert-success">
          {{ Session::get('deleted') }}
        </div>
      @endif
      @foreach($categories as $category)
        <div id="{{ $category->id }}">
          <h2 id="{{ $category->id }}">{{ trans("home.$category->name") }}</h2>
        </div>
        <div class="row">
        @foreach($subcategories->chunk(3) as $subcategoryChunk)
          <div class="row">
            @foreach($subcategoryChunk as $subcategory)
              @if($subcategory->category == $category->name)
                <div class="col-sm-6 col-md-4">
                  <a href="{{ route('topics', ['category' => $subcategory->name])}}">
                    <div class="thumbnail">
                      <img class="categories img-responsive" src="{{ asset($subcategory->img) }}" alt="...">
                      <div class="caption">
                        <h3>{{ trans("home.$subcategory->name") }}</h3>
                        <p>
                          {{ trans("home.$subcategory->description") }}
                        </p>
                        <span class="badge">
                          {{ trans('home.Topics') }}: {{ DB::table('topics')->where('category', $subcategory->name)->count() }}
                        </span>
                      </div>
                    </div>
                  </a>
                </div>
              @endif
            @endforeach
          </div>
        @endforeach
        </div>
      @endforeach
    </div>
    <div class="col-xs-2 categories">
      <ul class="nav nav-pills nav-stacked" >
        @foreach($categories as $category)
          <li id="{{ $category->id }}" class="scroll-menu"><a href="#{{ $category->id }}" class="menu-button">{{ trans("home.$category->name") }}</a></li>
        @endforeach
      </ul>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/home.js') }}"></script>
@endsection
