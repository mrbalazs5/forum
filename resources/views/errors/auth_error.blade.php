@extends('layouts.error')

@section('title')
  Admin Error
@endsection

@section('content')
  <h1>You need to sign in to access this page!</h1>
@endsection
