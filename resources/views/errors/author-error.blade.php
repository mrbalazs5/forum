@extends('layouts.error')

@section('title')
  Admin Error
@endsection

@section('content')
  <h1>You need to be the owner of this topic to delete it!</h1>
@endsection
