<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/error.css') }}">
  </head>
  <body>
    @yield('content')
  </body>
</html>
