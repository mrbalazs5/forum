<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="General Forum">
    <meta name="keywords" content="forum, topics, general">
    <meta name="author" content="Balázs Megyeri">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('pics/favicons/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet"  type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app2.css') }}">
    @yield('styles')
  </head>
  <body>
    <canvas class="canvas" id="myCanvas"></canvas>
    <header>
      @include('partials.header')
    </header>
    <div class="container">
      @yield('content')
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('js/canvas.js') }}" type="text/javascript"></script>
    @yield('scripts')
  </body>
</html>
