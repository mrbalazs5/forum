@extends('layouts.master')

@section('title')
  {{ trans('dashboard.Dashboard') }}
@endsection

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-2 menu">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ route('admin.dashboard') }}">{{ trans('dashboard.Dashboard') }}</a></li>
        <hr>
        <li><a href="{{ route('admin.users') }}">{{ trans('dashboard.Users') }}</a></li>
        <hr>
        <li><a href="{{ route('admin.topics') }}">{{ trans('dashboard.Topics') }}</a></li>
        <hr>
        <li><a href="{{ route('admin.comments') }}">{{ trans('dashboard.Comments') }}</a></li>
      </ul>
    </div>
    <div class="col-xs-10 menu">
      @yield('dashboard')
    </div>
  </div>
@endsection
