@extends('layouts.master')

@section('title')
  Sign In
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-4 col-xs-offset-4 categories">
      <h1>{{ trans('signin.Sign In') }}</h1>
      <hr>
      @if(count($errors) > 0)
        @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
        @endforeach
      @endif
      @if(Session::has('error'))
        <div class="alert alert-danger">
          {{ Session::get('error') }}
        </div>
      @endif
      <form action="{{ route('user.signin') }}" method="post">
        <div class="form-group">
          <label for="username">{{ trans('signin.Username') }}</label>
          <input type="text" class="form-control" id="username" name="username" required autofocus>
        </div>
        <div class="form-group">
          <label for="password">{{ trans('signin.Password') }}</label>
          <input type="password" class="form-control" id="password" name="password" required>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">{{ trans('signin.Sign In') }}</button>
      </form>
      <a href="{{ route('user.signup') }}" style="color: rgb(22, 151, 245)">{{ trans('signin.Don\'t you have an account?') }}</a>
  </div>
 </div>
 @endsection
