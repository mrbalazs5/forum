@extends('layouts.master')

@section('title')
  {{ trans('settings.Settings') }}
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-4 col-xs-offset-4 categories">
      <h1>{{ trans('settings.Settings') }}</h1>
      <hr>
      <h3>{{ trans('settings.Change Password') }}</h3>
      <hr>
      @if(count($errors) > 0)
        @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
        @endforeach
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success">
          {{ Session::get('success') }}
        </div>
      @elseif(Session::has('error'))
        <div class="alert alert-danger">
          {{ Session::get('error') }}
        </div>
      @endif
      <form action="{{ route('user.changepass') }}" method="post">
        <div class="form-group">
          <label for="oldpassword">{{ trans('settings.Old Password') }}</label>
          <input type="password" class="form-control" id="oldpassword" name="oldpassword" required>
        </div>
        <div class="form-group">
          <label for="newpassword">{{ trans('settings.New Password') }}</label>
          <input type="password" class="form-control" id="newpassword" name="newpassword" required>
        </div>
        <div class="form-group">
          <label for="renewpassword">{{ trans('settings.Re-enter New Password') }}</label>
          <input type="password" class="form-control" id="renewpassword" name="renewpassword" required>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success">{{ trans('settings.Change Password') }}</button>
      </form>
      <hr>
      <h3>{{ trans('settings.Change Avatar') }}</h3>
      <hr>
      <form action="{{ route('user.changeavatar') }}" method="post" enctype="multipart/form-data">
        <div class="form-group fileinput">
          <label for="newavatar">{{ trans('settings.New Avatar') }}</label>
          <input type="file" accept="image/*" id="newavatar" name="newavatar" required>
          <img class="img-responsive" id="pre_avatar1" alt="jpeg,png,jpg,gif,svg" />
          <p style="display: inline">{{ trans('settings.New Avatar') }}: </p>
          <img class="avatar" id="pre_avatar2"/>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-success">{{ trans('settings.Change Avatar') }}</button>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/settings.js') }}" charset="utf-8"></script>
@endsection
