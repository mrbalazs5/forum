@extends('layouts.master')

@section('title')
  {{ trans('comments.My Comments') }}
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-7 col-xs-offset-3 categories">
      <h1>{{ trans('comments.My Comments') }}</h1>
      <hr>
      @if(count($comments) > 0)
        @foreach($comments as $comment)
        <a href="{{ route('topic', ['name' => $comment->topic]) }}#{{ $comment->id }}">
          <div class="panel panel-default topics">
            <div class="panel-body">
              {{ $comment->comment }}
            </div>
            <div class="panel-footer">
              <strong>{{ trans('comments.Topic')}}: </strong> {{ $comment->topic }}<br>
              {{ $comment->created_at }}
            </div>
          </div>
        </a>
        @endforeach
      @else
        <h2>You don't have any comments yet!</h2>
      @endif
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/topics.js') }}" type="text/javascript"></script>
@endsection
