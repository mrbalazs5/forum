@extends('layouts.master')

@section('title')
  {{ trans('topics.My Topics') }}
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-7 col-xs-offset-3 categories">
      <h1>{{ trans('topics.My Topics') }}</h1>
      <hr>
      @if(count($topics) > 0)
        @foreach($topics as $topic)
        <a href="{{ route('topic', ['name' => $topic->name]) }}">
          <div class="panel panel-default topics">
            <div class="panel-body">
              {{ $topic->name }}
            </div>
            <div class="panel-footer">
              <strong>{{ trans('topics.Category') }}: </strong> {{ trans("home.$topic->category") }}<br>
              {{ $topic->created_at }}
            </div>
          </div>
        </a>
        @endforeach
      @else
        <h2>You don't have any topics yet!</h2>
      @endif
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/topics.js') }}" type="text/javascript"></script>
@endsection
