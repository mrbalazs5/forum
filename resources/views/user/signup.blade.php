@extends('layouts.master')

@section('title')
  Sign Up
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-4 col-xs-offset-4 categories">
      <h1>{{ trans('signup.Sign Up') }}</h1>
      <hr>
      @if(count($errors) > 0)
        @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
        @endforeach
      @endif
      <form action="{{ route('user.signup') }}" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="fname">{{ trans('signup.First Name') }}</label>
          <input type="text" class="form-control" id="fname" name="fname" autofocus required>
        </div>
        <div class="form-group">
          <label for="lname">{{ trans('signup.Last Name') }}</label>
          <input type="text" class="form-control" id="lname" name="lname" required>
        </div>
        <div class="form-group">
          <label for="email">{{ trans('signup.E-Mail') }}</label>
          <input type="text" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group">
          <label for="username">{{ trans('signup.Username') }}</label>
          <input type="text" class="form-control" id="username" name="username" placeholder="{{ trans('signup.Min 5, max 10 characters!') }}" required>
        </div>
        <div class="form-group">
          <label for="password">{{ trans('signup.Password') }}</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="{{ trans('signup.Min 6 characters!') }}" required>
        </div>
        <div class="form-group">
          <label for="repassword">{{ trans('signup.Re-enter Password') }}</label>
          <input type="password" class="form-control" id="repassword" name="repassword" placeholder="{{ trans('signup.Min 6 characters!') }}" required>
        </div>
        <div class="form-group fileinput">
          <label for="avatar">{{ trans('signup.Avatar') }}</label>
          <input type="file" accept="image/*" id="avatar" name="avatar" required>
          <img class="img-responsive" id="pre_avatar1" alt="jpeg,png,jpg,gif,svg" />
          <p style="display: inline">{{ trans('signup.Avatar') }}: </p>
          <img class="avatar" id="pre_avatar2"/>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default">{{ trans('signup.Sign Up') }}</button>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('js/signup.js') }}" charset="utf-8"></script>
@endsection
