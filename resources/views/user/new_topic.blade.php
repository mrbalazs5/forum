@extends('layouts.master')

@section('title')
  {{ trans('new-topic.New Topic') }}
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-4 col-xs-offset-4 categories">
      <h1>{{ trans('new-topic.New Topic') }}</h1>
      <hr>
      @if(count($errors) > 0)
        @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
        @endforeach
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success">
          {{ Session::get('success') }}
        </div>
      @endif
      <form action="{{ route('newtopic') }}" method="post">
        <div class="form-group">
          <label for="name">{{ trans('new-topic.Topic\'s Name') }}</label>
          <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group">
          <label for="category">{{ trans('new-topic.Topics\'s Category') }}</label>
          <select class="form-control" name="category" id="category" required>
            @foreach($categories as $category)
              <option value="{{ $category->name }}">{{trans("home.$category->name")}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="description">{{ trans('new-topic.Topic\'s Description') }}</label>
          <textarea class="form-control" rows="3" name="description" id="description" placeholder="{{ trans('new-topic.Say something about your topic') }}" required></textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success">{{ trans('new-topic.Add New Topic') }}</button>
      </form>
    </div>
  </div>
@endsection
