<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" id="brand" href="{{ route('home') }}" style="padding: 0; display: inline;"><img class="img-responsive" src="{{ asset('pics/banner.svg') }}" style="height: 60px; margin-top: 0; padding: 0;"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <a href="{{ route('lang.change',['to' => 'en']) }}"><img class="img-responsive lan" src="{{ asset('pics/lang/english.png') }}" alt=""></a>
        <a href="{{ route('lang.change',['to' => 'hu']) }}"><img class="img-responsive lan" src="{{ asset('pics/lang/hungarian.png') }}" alt=""></a>
        @if(Auth::check())
          @if(Auth::user()->permission == 'admin')
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> {{ trans('header.Dashboard') }}</a></li>
          @endif
          <li><a href="{{ route('newtopic') }}"><i class="fa fa-plus-square" aria-hidden="true"></i> {{ trans('header.New Topic') }}</a></li>
        @endif
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-address-card" aria-hidden="true"></i> {{ trans('header.User Management') }}<span class="caret"></span></a>
          <ul class="dropdown-menu">
            @if(Auth::check())
            <li><a href="{{ route('user.topics') }}"><i class="fa fa-window-restore" aria-hidden="true"></i> {{ trans('header.My Topics') }}</a></li>
            <li><a href="{{ route('user.comments') }}"><i class="fa fa-comment" aria-hidden="true"></i> {{ trans('header.My Comments') }}</a></li>
            <li><a href="{{ route('user.settings') }}"><i class="fa fa-cog" aria-hidden="true"></i> {{ trans('header.Settings') }}</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ route('user.logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{ trans('header.Logout') }}</a></li>
            @else
            <li><a href="{{ route('user.signin') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> {{ trans('header.Sign In') }}</a></li>
            <li><a href="{{ route('user.signup') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> {{ trans('header.Sign Up') }}</a></li>
            @endif
          </ul>
        </li>
        <?php
        use App\User;

        if(Auth::check()){
          $user = User::find(Auth::id());
          $avatar = asset($user->avatar);
        }
        elseif(!Auth::check()){
            $avatar = asset('pics/avatars/default.png');
        }

         ?>

        <img class="img-responsive nav-avatar" src="{{ $avatar }}">
      </ul>
    </div>
  </div>
</nav>
