@extends('layouts.master')

@section('title')
  {{ trans("home.$category") }}
@endsection

@section('content')
<div class="row">
  <div class="col-xs-7 col-xs-offset-3 categories">
    <h1>{{ trans("home.$category") }}</h1>
    <hr>
    @if(count($topics) > 0)
      @foreach($topics as $topic)
      <a href="{{ route('topic', ['name' => $topic->name]) }}">
        <div class="panel panel-default topics">
          <div class="panel-body">
            <h4>
              {{ $topic->name }}
            </h4>
            <p class="author">
              by {{ $topic->author }}
            </p>
          </div>
          <div class="panel-footer">
            <h5>
              {{ $topic->created_at }}
            </h5>
            <span class="badge">
              {{ trans('topics.Comments') }}: {{ DB::table('comments')->where('topic', $topic->name)->count() }}
            </span>
          </div>
        </div>
      </a>
      @endforeach
    @else
      <h2>{{ trans('topics.There aren\'t any topics in this category yet!') }}</h2>
    @endif
  </div>
</div>

@endsection

@section('scripts')
  <script src="{{ asset('js/topics.js') }}" type="text/javascript"></script>
@endsection
