@extends('layouts.master')

@section('title')
  {{ $topic->name }}
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-8 col-xs-offset-2 categories">
      <h1>{{ $topic->name }}</h1>
      <hr>
      <div class="panel panel-default head">
        <div class="panel-heading">
          <img class="img-responsive avatar" src="{{ asset(DB::table('users')->where('username', $topic->author)->value('avatar')) }}" alt="DELETED USER">
          <h3 class="panel-title">{{ $topic->author }}</h3>
        </div>
        <div class="panel-body">
          {{ $topic->description }}
        </div>
      </div>
      @if(Auth::check())
        @if(Auth::user()->username == $topic->author)
          <div class="panel panel-default">
            <div class="panel-body">
              <a href="{{ route('user.delete', ['author' => $topic->author, 'table' => 'topics', 'where' => 'name', 'what' => $topic->name]) }}" style="color: red">{{ trans('topic.Delete Topic') }}</a>
            </div>
          </div>
        @endif
      @endif
      <hr>
      @if(count($errors) > 0)
        @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
        @endforeach
      @endif
      @if(Auth::check())
      <form action="{{ route('comment',['topic' => $topic->name]) }}" method="post">
        <div class="form-group">
          <label for="comment">{{ trans('topic.Add Your Comment') }}</label>
          <textarea class="form-control" rows="3" name="comment" id="comment" placeholder="{{ trans('topic.Say something...') }}"></textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">{{ trans('topic.Comment') }}</button>
      </form>
      @else
        <div class="alert-info">
          {{ trans('topic.To comment, you need to sign in!') }}
        </div>
      @endif
      <hr>
      @if(count($comments) > 0)
        @foreach($comments as $comment)
          <div class="panel panel-default head" id="{{ $comment->id }}">
            <div class="panel-heading">
              <img class="img-responsive avatar" src="{{ asset(DB::table('users')->where('username', $comment->author)->value('avatar')) }}">
              <h2 class="panel-title">{{ $comment->author }}</h2>
              {{ $comment->created_at }}
            </div>
            <div class="panel-body">
              {{ $comment->comment }}
            </div>
          </div>
        @endforeach
      @else
        <h2>{{ trans('topic.There aren\'t any comments in this topic yet!') }}</h2>
      @endif
    </div>
  </div>
@endsection
