<?php

return [
  'Sign Up' => 'Regisztráció',
  'First Name' => 'Keresztnév',
  'Last Name' => 'Vezetéknév',
  'E-Mail' => 'Email cím',
  'Username' => 'Felhasználónév',
  'Password' => 'Jelszó',
  'Re-enter Password' => 'Jelszó újra',
  'Avatar' => 'Avatár',
  'Min 5, max 10 characters!' => 'Minimum 5, maximum 10 karakter!',
  'Min 6 characters!' => 'Minimum 6 karakter!'
];
