<?php

return [
  'New Topic' => 'Új téma',
  'Topic\'s Name' => 'Téma neve',
  'Topics\'s Category' => 'Téma kategóriája',
  'Topic\'s Description' => 'Téma leírása',
  'Say something about your topic' => 'Írj valamit a témádról',
  'Add New Topic' => 'Új téma hozzáadása'
];
