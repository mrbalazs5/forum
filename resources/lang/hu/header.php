<?php

return [
  'User Management' => 'Felhasználói beállítások',
  'New Topic' => 'Új téma',
  'Dashboard' => 'Vezérlőpult',
  'My Topics' => 'Témáim',
  'My Comments' => 'Kommentjeim',
  'Settings' => 'Beállítások',
  'Logout' => 'Kijelentkezés',
  'Sign In' => 'Bejelentkezés',
  'Sign Up' => 'Regisztráció'
];
