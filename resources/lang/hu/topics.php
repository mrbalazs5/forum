<?php

return [
    'My Topics' => 'Témáim',
    'Category' => 'Kategória',
    'Comments' => 'Kommentek',
    'There aren\'t any topics in this category yet!' => 'Még nincsenek témák ebben a kategóriában!'
];
