<?php

return [
  'Settings' => 'Beállítások',
  'Change Password' => 'Jelszó megváltoztatása',
  'Old Password' => 'Régi jelszó',
  'New Password' => 'Új jelszó',
  'Re-enter New Password' => 'Jelszó újra',
  'Change Avatar' => 'Avatár megváltoztatása',
  'New Avatar' => 'Új avatár'
];
