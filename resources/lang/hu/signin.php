<?php

return [
  'Sign In' => 'Bejelentkezés',
  'Username' => 'Felhasználónév',
  'Password' => 'Jelszó',
  'Don\'t you have an account?' => 'Még nem regisztráltál?'
];
