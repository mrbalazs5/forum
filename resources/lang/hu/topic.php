<?php

return [
  'Delete Topic' => 'Téma törlése',
  'To comment, you need to sign in!' => 'Hozzászóláshoz be kell jelentkezned!',
  'Add Your Comment' => 'Szólj hozzá',
  'Say something...' => 'Mondj valamit...',
  'Comment' => 'Komment közzététele',
  'There aren\'t any comments in this topic yet!' => 'Még senki se szólt hozzá ehhez a témához!'
];
