<?php

return [
  'Delete Topic' => 'Delete Topic',
  'To comment, you need to sign in!' => 'To comment, you need to sign in!',
  'Add Your Comment' => 'Add Your Comment',
  'Say something...' => 'Say something...',
  'Comment' => 'Comment',
  'There aren\'t any comments in this topic yet!' => 'There aren\'t any comments in this topic yet!'
];
