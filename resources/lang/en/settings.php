<?php

return [
  'Settings' => 'Settings',
  'Change Password' => 'Change Password',
  'Old Password' => 'Old Password',
  'New Password' => 'New Password',
  'Re-enter New Password' => 'Re-enter New Password ',
  'Change Avatar' => 'Change Avatar',
  'New Avatar' => 'New Avatar'
];
