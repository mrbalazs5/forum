<?php

return [
  'User Management' => 'User Management',
  'New Topic' => 'New Topic',
  'Dashboard' => 'Dashboard',
  'My Topics' => 'My Topics',
  'My Comments' => 'My Comments',
  'Settings' => 'Settings',
  'Logout' => 'Logout',
  'Sign In' => 'Sign In',
  'Sign Up' => 'Sign Up'
];
