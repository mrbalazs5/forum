<?php

return [
  'New Topic' => 'New Topic',
  'Topic\'s Name' => 'Topic\'s Name',
  'Topics\'s Category' => 'Topics\'s Category',
  'Topic\'s Description' => 'Topic\'s Description',
  'Say something about your topic' => 'Say something about your topic',
  'Add New Topic' => 'Add New Topic'
];
