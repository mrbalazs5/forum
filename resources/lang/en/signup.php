<?php

return [
  'Sign Up' => 'Sign Up',
  'First Name' => 'First Name',
  'Last Name' => 'Last Name',
  'E-Mail' => 'E-Mail',
  'Username' => 'Username',
  'Password' => 'Password',
  'Re-enter Password' => 'Re-enter Password',
  'Avatar' => 'Avatar',
  'Min 5, max 10 characters!' => 'Min 5, max 10 characters!',
  'Min 6 characters!' => 'Min 6 characters!'
];
