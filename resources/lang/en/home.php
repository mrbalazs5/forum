<?php

use App\Category;
use App\Subcategory;

$lang['welcome'] = 'Welcome! Choose a category!';
$lang['Topics'] = 'Topics';
$lang['Home'] = 'Home';
foreach (Category::all() as $category) {
  $lang[$category->name] = $category->name;
}
foreach (Subcategory::all() as $subcategory) {
  $lang[$subcategory->name] = $subcategory->name;
  $lang[$subcategory->description] = $subcategory->description;
}

return $lang;
