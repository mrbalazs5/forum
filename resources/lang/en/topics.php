<?php

return [
    'My Topics' => 'My Topics',
    'Category' => 'Category',
    'Comments' => 'Comments',
    'There aren\'t any topics in this category yet!' => 'There aren\'t any topics in this category yet!'
];
