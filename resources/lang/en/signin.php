<?php

return [
  'Sign In' => 'Sign In',
  'Username' => 'Username',
  'Password' => 'Password',
  'Don\'t you have an account?' => 'Don\'t you have an account?'
];
