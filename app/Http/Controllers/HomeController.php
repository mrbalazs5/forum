<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use Illuminate\Support\Facades\DB;
use App\Comment;
use Auth;
use JavaScript;
use Session;

/*
  handles routes connected with general things
*/
class HomeController extends Controller
{
    //get home view with categories and subcategories on it
    public function getHome(){
      $categories = Category::all();
      $subcategories = Subcategory::all();

      JavaScript::put([
          'cat_num' => $categories->count(),
      ]);

      return view('home',['categories' => $categories, 'subcategories' => $subcategories]);
    }

    //get error views
    public function getAuthError(){
      return view('errors.auth_error');
    }

    public function getAuthAdminError(){
      return view('errors.auth_admin_error');
    }

    public function getError404(){
      return view('errors.error404');
    }

    public function getError500(){
      return view('errors.error500');
    }

    public function getAuthorError(){
      return view('errors.author-error');
    }

    //language change
    public function getChangeLanguage($to){
      Session::put('loc', $to);
      Session::save();

      return redirect()->back();
    }

}
