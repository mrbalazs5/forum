<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use File;
use App\Topic;

/*
  handles routes connected with users
*/
class UserController extends Controller
{
    //get signup view
    public function getSignup(){
      return view('user.signup');
    }

    //validate registration form input and add a new user to the database
    public function postSignup(Request $request){

      $this->validate($request,[
        'fname' => 'required',
        'lname' => 'required',
        'email' => 'required|email|unique:users',
        'username' => 'required|min:5|max:10|unique:users',
        'password' => 'required|min:6',
        'repassword' => 'required|min:6|same:password',
        'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);

      $imageName = time().'.'.$request->avatar->getClientOriginalExtension();
      $request->avatar->move(public_path('pics/avatars'), $imageName);

      $user = new User([
          'fname' => $request->input('fname'),
          'lname' => $request->input('lname'),
          'email' => $request->input('email'),
          'username' => $request->input('username'),
          'password' => bcrypt($request->input('password')),
          'avatar' => 'pics/avatars/'.$imageName,
          'permission' => 'user',
          'topics' => DB::table('topics')->where('author', $request->input('username'))->count()
        ]
      );

      $user->save();

      Auth::login($user);

      return redirect()->route('home');
    }

    //get signin view
    public function getSignin(){
      return view('user.signin');
    }

    //validate login form input and log in user
    public function postSignin(Request $request){

      $this->validate($request, [
        'username' => 'required|min:5|max:10',
        'password' => 'required|min:6'
      ]);

      if(Auth::attempt([
        'username' => $request->input('username'),
        'password' => $request->input('password')
      ])){
        return redirect()->route('home');
      }

      return redirect()->back()->with('error','Inappropriate user data!');

    }

    //get the users setting view
    public function getSettings(){
      return view('user.settings');
    }

    //validate password change datas, and update the database
    public function postChangePassword(Request $request){
      $this->validate($request, [
        'oldpassword' => 'required|min:6',
        'newpassword' => 'required|min:6',
        'renewpassword' => 'required|min:6|same:newpassword'
      ]);
      if(Auth::attempt(['password' => $request->input('oldpassword')])){
        DB::table('users')->where('id', Auth::id())->update(['password' => bcrypt($request->input('newpassword'))]);
        return redirect()->back()->with('success','Your password has been changed!');
      }
      return redirect()->back()->with(['error', 'The given password doesn\'t match with your old password!']);
    }

    //validate avatar change datas, and update database
    public function postChangeAvatar(Request $request){
      $this->validate($request, [
        'newavatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);

      $user = User::find(Auth::id());

      File::delete($user->avatar);

      $imageName = time().'.'.$request->newavatar->getClientOriginalExtension();
      $request->newavatar->move(public_path('pics/avatars'), $imageName);

      DB::table('users')->where('id', Auth::id())->update(['avatar' => 'pics/avatars/'.$imageName]);
      return redirect()->back()->with('success','Your avatar has been changed!');
    }

    //log out the user
    public function getLogout(){
      Auth::logout();
      return redirect()->route('home');
    }

    //delete user from database
    public function getDelete($author, $table, $where, $what){
      if(Auth::user()->username == $author){
        DB::table($table)->where($where, $what)->delete();
        if($table == 'topics'){
          DB::table('comments')->where('topic', $what)->delete();
        }
        return redirect()->route('home')->with('deleted', 'Your topic has been deleted!');
      }
      return redirect()->route('auth.author.error');
    }

    //get topics view with topics
    public function getTopics(){
      $topics = DB::table('topics')->where('author', Auth::user()->username)->get();
      return view('user.topics', ['topics' => $topics]);
    }

    //get comments view with comments
    public function getComments(){
      $comments = DB::table('comments')->where('author', Auth::user()->username)->get();
      return view('user.comments', ['comments' => $comments]);
    }

}
