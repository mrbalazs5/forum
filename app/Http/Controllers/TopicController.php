<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use File;
use App\Topic;
use App\Comment;

/*
  handles routes connected with topics
*/
class TopicController extends Controller
{

  //get the new topics view
  public function getNewTopic(){
    $categories = DB::table('subcategories')->get();
    return view('user.new_topic', ['categories' => $categories]);
  }

  //add a new topic to the database
  public function postNewTopic(Request $request){
    $this->validate($request, [
      'name' => 'required|unique:topics',
      'category' => 'required',
      'description' => 'required'
    ]);

    $topic = new Topic([
      'name' => $request->input('name'),
      'author' => Auth::user()->username,
      'category' => $request->input('category'),
      'description' => $request->input('description')
    ]);

    $topic->save();

    DB::table('users')->where('username', Auth::user()->username)->increment('topics');

    return redirect()->back()->with('success', 'Topic has been created.');
  }

  //get topics view
  public function getTopics($category){
    $topics = DB::table('topics')->where('category', $category)->get();
    return view('topics.topics',['topics' => $topics, 'category' => $category]);
  }

  //get a single topic
  public function getTopic($name){
    $topic = DB::table('topics')->where('name', $name)->first();
    $comments = DB::table('comments')->where('topic', $name)->get();
    return view('topics.topic', ['topic' => $topic, 'comments' => $comments]);
  }

  //add a new comment to the database
  public function postComment(Request $request, $topic){
    $this->validate($request, [
      'comment' => 'required|max:7000'
    ]);

    $comment = new Comment([
      'author' => Auth::user()->username,
      'topic' => $topic,
      'comment' => $request->input('comment')
    ]);

    $comment->save();

    return redirect()->back();
  }
}
