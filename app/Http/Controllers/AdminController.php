<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use File;
use App\Topic;
use App\Comment;

/*
  handles routes connected with the admin dashboard
*/
class AdminController extends Controller
{

    //get dashboard view
    public function getDashboard(){
      return view('admin.dashboard');
    }

    // the dashboard's users section
    public function getUsers(){
        $users = User::all();
      return view('admin.users', ['users' => $users]);
    }

    // the dashboard's users section
    public function getOrder($table ,$orderby ,$order){
      $ordered = DB::table($table)->orderBy($orderby, $order)->get();

      return view("admin.$table", [$table => $ordered]);
    }

    //multifunctional delete method, protected by admin middleware
    public function getDelete($table, $where, $what){
      if($table == 'users'){
        File::delete(DB::table('users')->where('username', $what)->value('avatar'));
      }

      if($table == 'topics'){
        DB::table('comments')->where('topic', $what)->delete();
      }

      DB::table($table)->where($where, $what)->delete();

      return redirect()->back();
    }

    // change things in database
    public function getChange($table, $where, $is, $what, $to){
      DB::table($table)->where($where, $is)->update([$what => $to]);

      return redirect()->back();
    }

    //get topics view
    public function getTopics(){
      $topics = Topic::all();
      return view('admin.topics', ['topics' => $topics]);
    }

    //get comments view
    public function getComments(){
      $comments = Comment::all();

      return view('admin.comments', ['comments' => $comments]);
    }
}
