<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Lang;

/*
  set locale of the website
*/
class Language
{
    public function handle($request, Closure $next)
    {
      if(Session::has('loc')){
        Lang::setLocale(Session::get('loc'));
      }

        return $next($request);
    }
}
