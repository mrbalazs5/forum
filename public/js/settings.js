function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#pre_avatar1').attr('src', e.target.result);
          $('#pre_avatar2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#newavatar").change(function(){
    readURL(this);
});
