$(document).ready(function() {
  $(".topics").mouseenter(function(event) {
    $(this).css('box-shadow', '0 0 20px #9ecaed');
  });

  $(".topics").mouseleave(function(event) {
    $(this).css('box-shadow', 'none');
  });
});
