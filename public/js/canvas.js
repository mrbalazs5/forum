$(document).ready(function() {

  //initialize variables
  var canvas = document.getElementById("myCanvas");
  var ctx = canvas.getContext("2d");
  canvas.width  = window.innerWidth;
  canvas.height = window.innerHeight;
  var zoom = document.documentElement.clientWidth / window.innerWidth;
  var zoomNew;
  var posx; //mouse position x
  var posy; //mouse position y
  var cirq; //circle(star) quantity
  if(canvas.width <= 740 || canvas.height <= 120){
    cirq = 50;
  }else{
    cirq = 100;
  }


  //circle(stars) class
  var circle = function(x, y, dx, dy, size){
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.size = size;
    this.con = false;

    //draw the circles
    this.draw = function(){
      ctx.beginPath();
      ctx.fillStyle = "#FFFFFF";
      this.x += this.dx;
      this.y += this.dy;
      ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2);
      ctx.closePath();
      ctx.fill();

      if(this.y > canvas.height){
        this.dy = -this.dy;
      }
      if(this.x > canvas.width){
        this.dx = -this.dx;
      }
      if(this.y <= 0){
        this.dy = -this.dy;
      }
      if(this.x <= 0){
        this.dx = -this.dx;
      }
    };
  };

  //lines class
  var line = function(x1, y1, x2, y2){
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    //draw the lines
    this.draw = function(){
      ctx.beginPath();
      ctx.strokeStyle = "#FFFFFF";
      ctx.lineWidth=0.11;
      ctx.moveTo(x1,y1);
      ctx.lineTo(x2,y2);
      ctx.closePath();
      ctx.stroke();
    };
  };

  //define arrays for our objects
  circles = new Array(cirq);
  lines = new Array();

  //initialize circles array
  for(var i = 0; i < cirq; i++){
                                    //x                                                         //y                                         //dx                         //dy                                     //size
    circles[i] = new circle(Math.floor(Math.random() * canvas.width) + 0, Math.floor(Math.random() * canvas.height) + 0, (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (0.5 - 1) + 1));
  }

  //check for mousemove event
  $("#myCanvas").mousemove(function(event) {
    posx = event.pageX;
    posy = event.pageY;
  });

  //reload page on resize event
  $(window).resize(function(event) {
    canvas.width  = window.innerWidth;
    canvas.height = window.innerHeight;
    zoomNew = document.documentElement.clientWidth / window.innerWidth;
    if (zoom != zoomNew) {
      canvas.width  = window.innerWidth;
      canvas.height = window.innerHeight;

      for(var i = 0; i < cirq; i++){
                                        //x                                                         //y                                         //dx                         //dy                                     //size
        circles[i] = new circle(Math.floor(Math.random() * canvas.width) + 0, Math.floor(Math.random() * canvas.height) + 0, (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (0.5 - 1) + 1));
      }

        zoom = zoomNew
    }
    if(canvas.width <= 740 || canvas.height <= 120){
      cirq = 50;
    }else{
      cirq = 100;
    }

    for(var i = 0; i < cirq; i++){
                                      //x                                                         //y                                         //dx                         //dy                                     //size
      circles[i] = new circle(Math.floor(Math.random() * canvas.width) + 0, Math.floor(Math.random() * canvas.height) + 0, (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (-0.3 - 0.3) + 0.3), (Math.random() * (0.5 - 1) + 1));
    }
  });

  //check for changes in every 10 milliseconds
  setInterval(function(){
    ctx.clearRect(0,0, canvas.width,canvas.height);

    for(var i = 0; i < cirq; i++){
      circles[i].draw();
    }
      //check the distance between mouse and circles(stars) and make them connectable
      for(var i = 0; i < cirq; i++){
        if((Math.hypot((posx - circles[i].x),(posy - circles[i].y))) <= 200){
          circles[i].con = true;
        }
        else{
          circles[i].con = false;
        }
      }

    //draw lines between two circles(stars) if they are connectable and
    //their distance less than or equals 200px
    for(var i = 0; i < cirq-1; i++){
      for(var j = i + 1; j < cirq; j++){
        if((circles[i].con == true) && (circles[j].con == true) && ((Math.hypot((circles[i].x - circles[j].x),(circles[i].y - circles[j].y))) <= 150)){
          lines[i] = new line(circles[i].x, circles[i].y, circles[j].x, circles[j].y);
          lines[i].draw();
        }
      }
    }


  }, 10);

});
