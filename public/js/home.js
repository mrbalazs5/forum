//our check method, if scroll position is between the two
//specified headers then activate the specified button
function check(scp, cats, cat_num, sch, offh){
  for(var i = 0; i < cat_num; i++){
    for(var j = i + 1; j <= cat_num; j++){
      if((scp >= cats[i] - 5) && (scp <= cats[j] - 5)){
        $('.scroll-menu').removeClass('active');
        $('ul').find('#' + (i + 1)).addClass('active');
      }
    }
  }

}

$(document).ready(function() {

  $(".thumbnail").mouseenter(function(event) {
    $(this).css('box-shadow', '0 0 20px #9ecaed');
  });

  $(".thumbnail").mouseleave(function(event) {
    $(this).css('box-shadow', 'none');
  });

  $('.navbar-inverse').addClass('navbar-fixed-top');

  $('.scroll-menu').click(function(event) {
    $('.scroll-menu').removeClass('active');
    $(this).addClass('active');
  });

  //initialize variables
  var cats = [];
  var scp = $('div').find('#categories').scrollTop();
  var sch = (document.getElementById("categories").scrollHeight);
  var offh =(document.getElementById("categories").offsetHeight);

  //get the position of the categories(headers)
  for(var i = 0; i < cat_num; i++){
    cats[i] = $('div').find('#' + (i + 1)).position().top;
  }

  //get scroll position(scp)
  $('div').find('#categories').scroll(function(event) {
    scp = $('div').find('#categories').scrollTop();
  });

  //change some parameters on resize event
  $(window).resize(function(event) {
    $('div').find('#categories').scrollTop(0);
    scp = $('div').find('#categories').scrollTop();
    sch = (document.getElementById("categories").scrollHeight);
    offh =(document.getElementById("categories").offsetHeight);

    for(var i = 0; i < cat_num; i++){
      cats[i] = $('div').find('#' + (i + 1)).position().top;
    }
  });

  //check for changes in every millisecond and perform the changes
  setInterval(function(){
    check(scp, cats, cat_num, sch, offh);
  }, 1);

});
