<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Route::get('/', [
    'uses' => 'HomeController@getHome',
    'as' => 'home'
  ]);

  Route::get('/topics/{category}',[
    'uses' => 'TopicController@getTopics',
    'as' => 'topics'
  ]);

  Route::get('/topic/{name}',[
    'uses' => 'TopicController@getTopic',
    'as' => 'topic'
  ]);

  Route::post('/topic/comment/{topic}', [
    'uses' => 'TopicController@postComment',
    'as' => 'comment'
  ]);

  Route::get('/new-topic', [
    'middleware' => 'auth',
    'uses' => 'TopicController@getNewTopic',
    'as' => 'newtopic'
  ]);

  Route::post('/new-topic', [
    'middleware' => 'auth',
    'uses' => 'TopicController@postNewTopic',
    'as' => 'newtopic'
  ]);

  Route::get('/auth-error',[
    'uses' => 'HomeController@getAuthError',
    'as' => 'auth.error'
  ]);

  Route::get('/auth-admin-error',[
    'uses' => 'HomeController@getAuthAdminError',
    'as' => 'auth.admin.error'
  ]);

  Route::get('/error404',[
    'uses' => 'HomeController@getError404',
    'as' => 'error404'
  ]);

  Route::get('/error500',[
    'uses' => 'HomeController@getError500',
    'as' => 'error500'
  ]);

  Route::get('/author-error',[
    'uses' => 'HomeController@getAuthorError',
    'as' => 'auth.author.error'
  ]);

  Route::get('/lang/{to}',[
    'uses' => 'HomeController@getChangeLanguage',
    'as' => 'lang.change'
  ]);

//protect routes with admin and auth middlewares
Route::group(['prefix' => 'admin/dashboard', 'middleware' => ['auth', 'admin']], function(){

  Route::get('/comments', [
    'uses' => 'AdminController@getComments',
    'as' => 'admin.comments'
  ]);

  Route::get('/topics', [
    'uses' => 'AdminController@getTopics',
    'as' => 'admin.topics'
  ]);

  Route::get('/change/{table}/{where}/{is}/{what}/{to}', [
    'uses' => 'AdminController@getChange',
    'as' => 'admin.change'
  ]);

  Route::get('/delete/{table}/{where}/{what}', [
    'uses' => 'AdminController@getDelete',
    'as' => 'admin.delete'
  ]);

  Route::get('/order/{table}/{orderby}/{order}', [
    'uses' => 'AdminController@getOrder',
    'as' => 'admin.order'
  ]);

  Route::get('/users', [
    'uses' => 'AdminController@getUsers',
    'as' => 'admin.users'
  ]);

  Route::get('/', [
    'uses' => 'AdminController@getDashboard',
    'as' => 'admin.dashboard'
  ]);

});

Route::group(['prefix' => 'user'], function(){

  Route::get('/signup', [
    'middleware' => 'guest',
    'uses' => 'UserController@getSignup',
    'as' => 'user.signup'
  ]);

  Route::post('/signup', [
    'middleware' => 'guest',
    'uses' => 'UserController@postSignup',
    'as' => 'user.signup'
  ]);

  Route::get('/signin', [
    'middleware' => 'guest',
    'uses' => 'UserController@getSignin',
    'as' => 'user.signin'
  ]);

  Route::post('/signin', [
    'middleware' => 'guest',
    'uses' => 'UserController@postSignin',
    'as' => 'user.signin'
  ]);

  Route::get('/logout', [
    'middleware' => 'auth',
    'uses' => 'UserController@getLogout',
    'as' => 'user.logout'
  ]);

  Route::get('/settings', [
    'middleware' => 'auth',
    'uses' => 'UserController@getSettings',
    'as' => 'user.settings'
  ]);

  Route::post('/change-password', [
    'middleware' => 'auth',
    'uses' => 'UserController@postChangePassword',
    'as' => 'user.changepass'
  ]);

  Route::post('/change-avatar', [
    'middleware' => 'auth',
    'uses' => 'UserController@postChangeAvatar',
    'as' => 'user.changeavatar'
  ]);

  Route::get('/delete/{author}/{table}/{where}/{what}', [
    'middleware' => 'auth',
    'uses' => 'UserController@getDelete',
    'as' => 'user.delete'
  ]);

  Route::get('/topics', [
    'middleware' => 'auth',
    'uses' => 'UserController@getTopics',
    'as' => 'user.topics'
  ]);

  Route::get('/comments', [
    'middleware' => 'auth',
    'uses' => 'UserController@getComments',
    'as' => 'user.comments'
  ]);

});
