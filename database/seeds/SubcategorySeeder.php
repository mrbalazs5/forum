<?php

use Illuminate\Database\Seeder;
use App\Subcategory;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/chat.jpg',
          'name' => 'Chat',
          'description' => 'Meet new people. Talk with each other.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/clubs.png',
          'name' => 'Clubs',
          'description' => 'Club forums for club members.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/fun.jpg',
          'name' => 'Fun',
          'description' => 'Talk about anything, for example jokes, stories...'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/cinema.jpg',
          'name' => 'Movies',
          'description' => 'Movies, cinema, actors...'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/games.jpg',
          'name' => 'Video Games',
          'description' => 'Talk about games, new releases, retro games etc.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'General',
          'img' => 'pics/categories/hate.jpg',
          'name' => 'Haters',
          'description' => 'If you hate something you can write it here and maybe you will get some followers.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Lifestyle',
          'img' => 'pics/categories/babies.png',
          'name' => 'Baby rooms',
          'description' => 'Venue for happy mothers and fathers who wants to talk about their babies.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Lifestyle',
          'img' => 'pics/categories/kitchen.jpg',
          'name' => 'Kitchen',
          'description' => 'A place where you can share recipes, diets, ideas.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Lifestyle',
          'img' => 'pics/categories/celeb.png',
          'name' => 'Celebrity',
          'description' => 'Talk about celebrities.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Economy, businnes, career, job',
          'img' => 'pics/categories/bank.jpg',
          'name' => 'Banks',
          'description' => 'Banks, financial institutions, credit.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Economy, businnes, career, job',
          'img' => 'pics/categories/career.jpg',
          'name' => 'Career, job',
          'description' => 'Job offers, career building tips.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Economy, businnes, career, job',
          'img' => 'pics/categories/stock.jpg',
          'name' => 'Stock exchange, investments, taxes, companies',
          'description' => 'Forcasts, stock exchange, tax problems, tips for contractors.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Hobby, free time',
          'img' => 'pics/categories/travel.jpg',
          'name' => 'Travel',
          'description' => 'Forum for travellers and wannabe travellers.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Hobby, free time',
          'img' => 'pics/categories/animals.jpg',
          'name' => 'Animals',
          'description' => 'Talk about pets, feeding, wild animals or hunting.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Hobby, free time',
          'img' => 'pics/categories/horticulture.jpg',
          'name' => 'Horticulture',
          'description' => 'Plants, seeds, breeding, garden tools etc.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Culture, arts',
          'img' => 'pics/categories/literature.jpg',
          'name' => 'Literature',
          'description' => 'About books, poems, literature.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Culture, arts',
          'img' => 'pics/categories/paint.jpg',
          'name' => 'Painting, sculpture',
          'description' => 'Show your creations, discuss about paintings and sculptures.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Culture, arts',
          'img' => 'pics/categories/music.jpeg',
          'name' => 'Music',
          'description' => 'Talk about music, songs, performers, concerts.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Politics, public life',
          'img' => 'pics/categories/politics.jpg',
          'name' => 'Current politics',
          'description' => 'Current political events, politicans.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Politics, public life',
          'img' => 'pics/categories/economy.png',
          'name' => 'Economic policy',
          'description' => 'Current economic climate, economy.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Politics, public life',
          'img' => 'pics/categories/history.png',
          'name' => 'Cultural policy',
          'description' => 'History, religion, local politics.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Science, technics',
          'img' => 'pics/categories/it.jpg',
          'name' => 'Information Technology',
          'description' => 'Topics about IT, computers, algoritms, mathematic etc.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Science, technics',
          'img' => 'pics/categories/electronics.jpg',
          'name' => 'Electronics',
          'description' => 'Radios, televisions, computers etc.'
        ]);

        $subcategory->save();

        $subcategory = new Subcategory([
          'category' => 'Science, technics',
          'img' => 'pics/categories/science.jpg',
          'name' => 'General science',
          'description' => 'Theories, physics, chemistry etc.'
        ]);

        $subcategory->save();
    }
}
