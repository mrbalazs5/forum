<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category([
          'name' => 'General'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Lifestyle'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Economy, businnes, career, job'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Hobby, free time'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Culture, arts'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Politics, public life'
        ]);

        $category->save();

        $category = new Category([
          'name' => 'Science, technics'
        ]);

        $category->save();
    }
}
